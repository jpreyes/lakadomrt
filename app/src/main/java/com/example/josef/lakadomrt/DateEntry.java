package com.example.josef.lakadomrt;
import android.text.format.Time;

/**
 * Created by Josef on 4/25/2015.
 */
public class DateEntry {
    private int id;
    private String time;
    private boolean sira;

    public DateEntry(){}

    public DateEntry(String time, boolean sira){
        super();
        this.time = time;
        this.sira= sira;

    }

    public String getTime(){
        return this.time;
    }

    public boolean getSira(){
        return this.sira;
    }
}

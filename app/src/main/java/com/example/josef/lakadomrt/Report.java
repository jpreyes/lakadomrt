package com.example.josef.lakadomrt;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Kelly on 4/25/2015.
 */
public class Report extends ActionBarActivity {

    boolean cb1value, cb2value, cb3value, cb4value, cb5value;
    CheckBox cb1, cb2, cb3, cb4, cb5;
    Button submitButton;
    String sName;
    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.report_layout);
        Intent get = getIntent();
        sName = get.getStringExtra("station");


        submitButton = (Button) findViewById(R.id.submit_button);
        cb1 = (CheckBox) findViewById(R.id.cb1);
        cb2 = (CheckBox) findViewById(R.id.cb2);
    //    cb3 = (CheckBox) findViewById(R.id.cb3);
        cb4 = (CheckBox) findViewById(R.id.cb4);
        cb5 = (CheckBox) findViewById(R.id.cb5);


        submitButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                cb1value = cb1.isChecked();
                cb1.setChecked(false);
                cb2value = cb2.isChecked();
                cb2.setChecked(false);
             //   cb3value = cb3.isChecked();
              //  cb3.setChecked(false);
                cb4value = cb4.isChecked();
                cb4.setChecked(false);
                cb5value = cb5.isChecked();
                cb5.setChecked(false);
                String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
                ((MyApplication)getApplication()).db.increment(new DateEntry(currentDateTimeString,cb1value));
                int v2 = cb2value ? 1:0;
                int v4 = cb4value ? 1:0;
                int v5 = cb5value ? 1:0;
                System.out.println("ADD PILA " + v4);
                ((MyApplication)getApplication()).db2.addReport(new ReportEntry(sName,v2, v4, v5 ));
                finish();
            }
        });


    }
}

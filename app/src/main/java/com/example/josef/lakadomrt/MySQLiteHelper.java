package com.example.josef.lakadomrt;

/**
 * Created by Josef on 4/25/2015.
 */
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.content.ContentValues;
import android.database.Cursor;
import android.widget.Toast;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "DateDatabase";

    public MySQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table

        String CREATE_DATE_TABLE = "CREATE TABLE dates ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "time TEXT, "+
                "sira INTEGER )";

        // create books table
        db.execSQL(CREATE_DATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS dates");

        // create fresh books table
        this.onCreate(db);
    }

    private static final String TABLE_DATES = "dates";

    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String TIME = "time";
    private static final String SIRA = "sira";

    private static final String[] COLUMNS = {KEY_ID,TIME,SIRA};

    public void increment(DateEntry de){
        // 1. get reference to writable DB
      SQLiteDatabase db = this.getWritableDatabase();
        Log.d("addBook", de.toString());

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(TIME, de.getTime()); // get title
        values.put(SIRA, de.getSira()); // get author

        // 3. insert
        db.insert(TABLE_DATES, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }
    public int numSira(){
        // 1. get reference to writable DB
        int num=0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT  * FROM " + TABLE_DATES;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                if(cursor.getInt(2)== 1){
                    num++;
                }
            }while(cursor.moveToNext());
        }
        System.out.println("asdf");
        db.close();
        return num;
    }

}

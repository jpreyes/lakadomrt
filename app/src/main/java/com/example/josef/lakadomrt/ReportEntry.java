package com.example.josef.lakadomrt;
import android.text.format.Time;

/**
 * Created by Josef on 4/25/2015.
 */
public class ReportEntry {
    private int id;
    private String station;
    private String time;
    private int stop;
    private int pila;
    private int air;

    public ReportEntry(){}

    public ReportEntry(String station, int stop, int pila, int air){
        super();
        this.station= station;
    //    this.time = time;
        this.stop= stop;
        this.pila=pila;
        this.air=air;

    }

   // public String getTime(){
    //    return this.time;
    //}

    public String getStation(){
        return this.station;
    }
    public int getStop(){return this.stop;}
    public int getPila(){ return this.pila;}
    public int getAir() {return this.air;}
}

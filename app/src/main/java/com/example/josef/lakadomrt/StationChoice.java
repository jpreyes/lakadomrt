package com.example.josef.lakadomrt;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Kelly on 4/25/2015.
 */
public class StationChoice extends ActionBarActivity {

    boolean nakarating = false;
    boolean isNorth = true;
    boolean isStopped = false;
    TextView stationTextView, boundTextView, waitTimeTextView, trainArrivalTextView, usersTextView, habaPilaTextView, siraAirconTextView;
    Button datingSakayButton, reportButton;

    Color grn = new Color();


    String sName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.station_choice);

        TextView stationTextView = (TextView) findViewById(R.id.station_name);
        Intent i = getIntent();
        final String stationName = i.getStringExtra("station");
        stationTextView.setText(stationName);
        sName = stationName;

        datingSakayButton = (Button) findViewById(R.id.dating_sakay);

        datingSakayButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (nakarating == false) {
                    datingSakayButton.setText("Nakasakay");
                    nakarating = true;
                } else if (nakarating == true) {
                    datingSakayButton.setText("Waiting");
                    nakarating = false;
                }
            }
        });


        reportButton = (Button) findViewById(R.id.report);

        reportButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(), Report.class);
                i.putExtra("station", stationName);
                startActivity(i);
            }
        });

        waitTimeTextView = (TextView) findViewById(R.id.ave_waiting_time);
        trainArrivalTextView = (TextView) findViewById(R.id.train_arrival);
        usersTextView = (TextView) findViewById(R.id.number_of_users);
        habaPilaTextView = (TextView) findViewById(R.id.mahaba_pila);
        siraAirconTextView = (TextView) findViewById(R.id.sira_aircon);
        boundTextView = (TextView) findViewById(R.id.bound_type_label);

        boundTextView.setText("Northbound");
        waitTimeTextView.setText("00:20:00");
        trainArrivalTextView.setText("00:05:00");
        int numberPeople = ((MyApplication) getApplication()).db2.numStation(stationName);
        usersTextView.setText(String.valueOf(numberPeople));
        if (((MyApplication) getApplication()).db2.numPila(stationName) == 1) {
            habaPilaTextView.setText("Mahaba");
        } else {
            habaPilaTextView.setText("Hindi");
        }
        if (((MyApplication) getApplication()).db2.numAir(stationName) == 1) {
            siraAirconTextView.setText("Oo");
        }
        {
            siraAirconTextView.setText("No");
        }


        if (((MyApplication) getApplication()).db2.numStop(stationName) == 0) {
            boundTextView.setTextColor(grn.rgb(0, 150, 5));
        } else {
            boundTextView.setTextColor(Color.RED);
        }

        boundTextView.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (isNorth == true) {
                    isNorth = false;
                    boundTextView.setText("Southbound");
                    waitTimeTextView.setText("00:15:40");
                    trainArrivalTextView.setText("00:07:00");
                    int numberPeople = ((MyApplication) getApplication()).db2.numStation(stationName);
                    usersTextView.setText(String.valueOf(numberPeople));
                    if (((MyApplication) getApplication()).db2.numPila(stationName) == 1) {
                        habaPilaTextView.setText("Mahaba");
                    } else {
                        habaPilaTextView.setText("Hindi");
                    }
                    if (((MyApplication) getApplication()).db2.numAir(stationName) == 1) {
                        siraAirconTextView.setText("Oo");
                    }
                    {
                        siraAirconTextView.setText("No");
                    }

                } else if (isNorth == false) {
                    isNorth = true;
                    boundTextView.setText("Northbound");
                    waitTimeTextView.setText("00:20:00");
                    trainArrivalTextView.setText("00:05:00");
                    int numberPeople = ((MyApplication) getApplication()).db2.numStation(stationName);
                    usersTextView.setText(String.valueOf(numberPeople));
                    if (((MyApplication) getApplication()).db2.numPila(stationName) == 1) {
                        habaPilaTextView.setText("Mahaba");
                    } else {
                        habaPilaTextView.setText("Hindi");
                    }
                    if (((MyApplication) getApplication()).db2.numAir(stationName) == 1) {
                        siraAirconTextView.setText("Oo");
                    }
                    {
                        siraAirconTextView.setText("No");
                    }
                }
            }
        });

    }

}

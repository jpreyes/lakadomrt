package com.example.josef.lakadomrt;

import android.app.Application;
import android.content.res.Configuration;

import android.text.format.Time;

import java.text.DateFormat;
import java.util.Date;

/**
 * Created by Josef on 4/25/2015.
 */
public class MyApplication extends Application {

    MySQLiteHelper db;
    MySQLiteHelper2 db2;
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = new MySQLiteHelper(this);
        db2 = new MySQLiteHelper2(this);
        Time now = new Time();
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}

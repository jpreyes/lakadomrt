package com.example.josef.lakadomrt;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Josef on 4/25/2015.
 */
public class MySQLiteHelper2 extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "ReportDB";

    public MySQLiteHelper2(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // SQL statement to create book table

        String CREATE_DATE_TABLE = "CREATE TABLE reports ( " +
                "id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "station TEXT, "+
                "time TEXT, " +
                "stop INTEGER, " +
                "tao INTEGER, "+
                "pila INTEGER," +
                "air INTEGER )";

        // create books table
        db.execSQL(CREATE_DATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older books table if existed
        db.execSQL("DROP TABLE IF EXISTS dates");

        // create fresh books table
        this.onCreate(db);
    }

    private static final String TABLE_REPORTS = "reports";

    // Books Table Columns names
    private static final String KEY_ID = "id";
    private static final String STATION = "station";
    private static final String TIME = "time";
    private static final String STOP = "stop";
    private static final String TAO = "tao";
    private static final String PILA = "pila";
    private static final String AIR = "air";

    private static final String[] COLUMNS = {KEY_ID,STATION, STOP, TAO, PILA, AIR};

    public void addReport(ReportEntry de){
        // 1. get reference to writable DB
        SQLiteDatabase db = this.getWritableDatabase();
        Log.d("addBook", de.toString());

        // 2. create ContentValues to add key "column"/value
        ContentValues values = new ContentValues();
        values.put(STATION, de.getStation()); // get title
     //   values.put(TIME, de.getTime()); // get author
        values.put(STOP, de.getStop()); // get author
        values.put(PILA, de.getPila()); // get author
        values.put(AIR, de.getAir()); // get author

        // 3. insert
        db.insert(TABLE_REPORTS, // table
                null, //nullColumnHack
                values); // key/value -> keys = column names/ values = column values

        // 4. close
        db.close();
    }

    public int numStation(String station){
        int num=0;
        SQLiteDatabase db= this.getWritableDatabase();
        String query = " SELECT * FROM " + TABLE_REPORTS;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                if(cursor.getString(1).equals(station)) {
                    num++;
                }

            }while(cursor.moveToNext());
        }
        return num;
    }

    public int numStop(String station) {
        int num = 0;
        int total = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = " SELECT * FROM " + TABLE_REPORTS;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                if(cursor.getString(1).equals(station)) {
                    if (cursor.getInt(3) == 1) {
                        num++;
                    }
                    total++;
                }

            } while (cursor.moveToNext());
        }
        if ((num * 2) > total) {
            return 1;
        } else {
            return 0;
        }
    }
    public int numPila(String station) {
        int num = 0;
        int total = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = " SELECT * FROM " + TABLE_REPORTS;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            System.out.print("CURRENT STATION:" + station);
            do {
                if(cursor.getString(1).equals(station)) {
                    System.out.println(cursor.getInt(4));
                    if (cursor.getInt(4) == 1) {
                        num++;
                    }
                    total++;
                }
                   System.out.println(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        System.out.println(num);
        if ((num * 2) > total) {
            return 1;
        } else {
            return 0;
        }
    }
    public int numTao(String station) {
        int num = 0;
        int total = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = " SELECT * FROM " + TABLE_REPORTS + " WHERE STATION IS " + station;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                if (cursor.getInt(5) == 1) {
                    num++;
                }
                total++;

            } while (cursor.moveToNext());
        }
        if ((num * 2) > total) {
            return 1;
        } else {
            return 0;
        }
    }
    public int numAir(String station){
        int num=0;
        int total=0;
        SQLiteDatabase db= this.getWritableDatabase();
        String query = " SELECT * FROM " + TABLE_REPORTS;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            do{
                if(cursor.getString(1).equals(station)) {
                    if (cursor.getInt(5) == 1) {
                        num++;
                    }
                    total++;
                }

            }while(cursor.moveToNext());
        }
        if((num*2)>total){
            return 1;
        }else{
            return 0;
        }
    }/*
   0 private int id;
   1 private String station;
   2 private String time;
   3 private boolean stop;
   4 private boolean tao;
   5 private boolean pila;
   6 private boolean air;
*/


}

package com.example.josef.lakadomrt;

/**
 * Created by Kelly on 4/25/2015.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.app.ListActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class StationList extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.station_list);

        //storing resources into Array
        String[] stations = getResources().getStringArray(R.array.stations);

        //binding resources Array to ListAdapter
        this.setListAdapter(new ArrayAdapter<String>(this, R.layout.station_list, R.id.station, stations));

        ListView lv = getListView();

        lv.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String station = ((TextView)view).getText().toString();

                Intent i = new Intent(getApplicationContext(), StationChoice.class);

                i.putExtra("station", station);
                startActivity(i);
            }
        });
    }


}
